Changelog for the Tango theme for the Claws Mail project.
---------------------------------------------------------

2009-09-06: version 1.2.1
    * Changed licensing to public domain. This theme is now
    based on Tango 0.8.90 or later, which became public domain.

2008-09-23: version 1.2, Claws Mail 3.5.0cvs78+, 3.6.0
    * Added key_gpg_signed.png.
    * Resized all tray icons from 16x16 to 24x24.
    * Made tray icons that signal new messages stand out more.

2008-07-01: version 1.1, Claws Mail 3.5.0
    * Added cancel.png, delete_btn.png, mail_reply_to_list.png.

2008-05-09: version 1.0, Claws Mail 3.4.0
    * Initial version.

END OF FILE