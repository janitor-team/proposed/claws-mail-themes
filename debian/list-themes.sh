#!/bin/sh
test "x$1" = "x" && echo "Missing themes directory argument" && exit 1
find "$1" -maxdepth 1 -type d \
	| sed "s,$1,," \
	| sort \
	| sed "s,^,\\\',;s,$,\\\'\,, " \
	| xargs
